import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://147.139.138.245:3000/')

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/a_Masuk'))

WebUI.setText(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/input_Masukkan No. Ponsel_phoneNumber'), 
    '081273216164')

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/button_Masuk'))

WebUI.setText(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/input_Verifikasi OTP_otpCode'), 
    '123456')

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/a_Kendaraan'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/a_Aksesoris'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/a_Sparepart'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/a_Dealer Promo'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/a_Kendaraan'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/label_New Avanza'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/div_New Venturer'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/label_New Venturer'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/label_New Kijang Innova'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/div_New Agya'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/label_New Agya'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/label_New Avanza'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/label_New Avanza'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/img'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/img'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/a_Akun Saya'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/a_Aksesoris'))

WebUI.click(findTestObject('Object Repository/Check Halaman/Page_TOSS Auto 2000 - OFFICIAL TOYOTA ECOMMERCE/a_Jasa Servis'))

