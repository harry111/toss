<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Bayar Sekarang</name>
   <tag></tag>
   <elementGuidId>fc3d7117-c680-4e7e-a833-3603adaeae54</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//button[@type='button'])[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.ps-btn.ps-btn--primary</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5bc110c7-1933-4c63-8583-35e011d4c6f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>06f73301-b37b-462e-ae82-d6ef01aa6450</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ps-btn ps-btn--primary</value>
      <webElementGuid>94e9497e-e01b-4dea-bd3b-8e0f0ee943b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Bayar Sekarang</value>
      <webElementGuid>e4f57f9d-662e-449c-82d6-5b460787445a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;sticky-product-price&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-8 d-flex flex-v-align-center justify-between&quot;]/div[2]/button[@class=&quot;ps-btn ps-btn--primary&quot;]</value>
      <webElementGuid>6b9c811e-e3fd-4871-85ba-658f4fbd9869</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//button[@type='button'])[2]</value>
      <webElementGuid>9b1b7fbb-9506-40b6-ba24-efdd716206f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div[9]/div/div/div/div/div[2]/div[2]/button</value>
      <webElementGuid>53a697a2-00d4-40eb-a00c-efe3eda35065</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 7.500.000'])[3]/following::button[1]</value>
      <webElementGuid>e5a05f22-a2ba-4089-8276-79a05aa92315</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Masuk ke ', '&quot;', 'Pengaturan', '&quot;', '')])[1]/preceding::button[2]</value>
      <webElementGuid>adf704a9-935f-4f26-b4cf-45e9214e3397</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Ketik ', '&quot;', 'Lokasi', '&quot;', ' di kotak pencarian')])[1]/preceding::button[2]</value>
      <webElementGuid>9bad7e44-755f-45c1-9214-3eee1b1d4c5f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Bayar Sekarang']/parent::*</value>
      <webElementGuid>2529c8ad-e95b-4160-a7f0-7371c9264e65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>f607ff83-8075-4d85-961d-89e18d898397</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = 'Bayar Sekarang' or . = 'Bayar Sekarang')]</value>
      <webElementGuid>1fc23205-07e3-4fda-9e6a-aed4c943f21b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
