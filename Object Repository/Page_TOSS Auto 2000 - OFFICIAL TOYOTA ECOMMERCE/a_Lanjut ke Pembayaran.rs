<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Lanjut ke Pembayaran</name>
   <tag></tag>
   <elementGuidId>ed58c16c-58c4-43ad-b923-b16b59584c14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/div[5]/div/div[2]/div/div/div[2]/div/div/div/div[2]/div[4]/div/p[3]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.ps-btn.ps-btn--primary.btn.button-blue-primary.text-white.radius-50.line-56</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>9b72cd8b-3a53-4e89-9ef8-063db3ed5f07</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ps-btn ps-btn--primary btn button-blue-primary text-white radius-50 line-56</value>
      <webElementGuid>070a7e85-adcc-4c1a-a30e-e8614f0cd55f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Lanjut ke Pembayaran</value>
      <webElementGuid>49ec9b52-c8ff-4bc9-8ef2-6c4aaf338ebe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;ps-page&quot;]/div[@class=&quot;ps-faqs&quot;]/div[@class=&quot;ps-post ps-post--full&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;ps-post__content checkout-content&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;mb-5 checkout-lengkapi-data-content&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-4&quot;]/div[@class=&quot;card p-4 radius-10 margin-top&quot;]/div[@class=&quot;card-body mb-3 rounded ringkasan-biaya-wrapper&quot;]/p[@class=&quot;text-center d-block&quot;]/a[@class=&quot;ps-btn ps-btn--primary btn button-blue-primary text-white radius-50 line-56&quot;]</value>
      <webElementGuid>9c05bec9-7034-4d8c-8710-d169b4fc1b9a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div[5]/div/div[2]/div/div/div[2]/div/div/div/div[2]/div[4]/div/p[3]/a</value>
      <webElementGuid>a8f32519-96d7-4174-be1e-07825e0c023b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Lanjut ke Pembayaran')]</value>
      <webElementGuid>86186071-aaca-4042-9c1b-8a3b88839055</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 7.500.000'])[5]/following::a[1]</value>
      <webElementGuid>e932a666-8172-4581-9ebd-0c0e2a40f7e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total Biaya Pembayaran'])[1]/following::a[1]</value>
      <webElementGuid>7dbc141c-e1f8-4ad6-83d3-56b0b069ad39</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Metode Pembayaran'])[1]/preceding::a[1]</value>
      <webElementGuid>c65c41ba-059c-4aa8-a425-3a02f1a551b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/preceding::a[1]</value>
      <webElementGuid>22672319-7b8e-4bc5-ad00-ada6bfc59e3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lanjut ke Pembayaran']/parent::*</value>
      <webElementGuid>176729fa-5258-4ae2-a443-1bf35edd5969</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p[3]/a</value>
      <webElementGuid>19fb28fe-7416-4f65-a9d8-919aed79364e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = 'Lanjut ke Pembayaran' or . = 'Lanjut ke Pembayaran')]</value>
      <webElementGuid>f1e1ed5d-a875-4036-8e24-e09b9cf50aa4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
