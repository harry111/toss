<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Masukkan No. Ponsel_phoneNumber</name>
   <tag></tag>
   <elementGuidId>6a2d5ed0-1f56-44a9-9453-ebbe94ecd311</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#phoneNumber</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='phoneNumber']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>fa8c111d-a620-4691-9697-2a11244019a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>tel</value>
      <webElementGuid>9f107f04-bc3e-44b8-83ac-a8336c5c0475</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>6217118a-8c63-4c74-956b-f37f38d0fbbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>phoneNumber</value>
      <webElementGuid>5b2fa234-8221-4742-9799-3f0b12b9fd58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Misal, 0810000000</value>
      <webElementGuid>081d230e-39aa-4f75-adf1-2bb001fff03c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-type</name>
      <type>Main</type>
      <value>number</value>
      <webElementGuid>25468954-5114-46d0-9a56-3c7407a4aa48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>081273216164</value>
      <webElementGuid>73b3840b-e787-445f-a6c9-0a8f28ec5d86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;phoneNumber&quot;)</value>
      <webElementGuid>c41af1b1-a4a7-4d54-ad1f-60fb5145e4e9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='phoneNumber']</value>
      <webElementGuid>a753dcbe-8a31-4032-811b-e2f8971e760e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div[3]/div/div/div/form/div/div/input</value>
      <webElementGuid>b32353a6-f1ba-46da-8f92-7c052c53d1b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>7aa8033b-2f4b-4dc3-89f0-8264a4a896f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'tel' and @id = 'phoneNumber' and @placeholder = 'Misal, 0810000000']</value>
      <webElementGuid>26b4ca3d-b308-4720-98ce-3fce3d5bac96</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
