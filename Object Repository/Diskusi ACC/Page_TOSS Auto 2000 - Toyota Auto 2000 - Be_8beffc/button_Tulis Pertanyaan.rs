<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Tulis Pertanyaan</name>
   <tag></tag>
   <elementGuidId>700e04dd-2c7f-436a-a97b-0cf41142ecac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.radius-50.ps-btn--primary.px-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='discussion-content']/div/section[2]/div[2]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7bb1e1c9-93b4-4225-b977-99c1cdb5eb2b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn radius-50 ps-btn--primary px-4</value>
      <webElementGuid>38c63a62-3ea5-47fd-9175-94e6232b8dd2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Tulis Pertanyaan</value>
      <webElementGuid>c9cadd2a-1b39-4cc7-80eb-d3cb6816d6d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;discussion-content&quot;)/div[@class=&quot;ps-desc&quot;]/section[@class=&quot;discussion-well-segment&quot;]/div[@class=&quot;w-100 flex-between v-center m-flex-column&quot;]/button[@class=&quot;btn radius-50 ps-btn--primary px-4&quot;]</value>
      <webElementGuid>e1f03bfa-9a54-45a1-89a3-2c9b353d1c4d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='discussion-content']/div/section[2]/div[2]/button</value>
      <webElementGuid>2f869738-973a-42bb-89e7-d87aef53f426</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskusikan dengan penjual atau pengguna lain'])[1]/following::button[1]</value>
      <webElementGuid>254c722a-2823-426f-8a51-4c19d71a4467</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ada pertanyaan?'])[1]/following::button[1]</value>
      <webElementGuid>668969e9-f6c9-41ca-bfce-283c9f1ec8b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='test'])[2]/preceding::button[2]</value>
      <webElementGuid>486ff47f-397d-4a1d-b25e-506c3dff4418</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Min. 10 karakter'])[1]/preceding::button[3]</value>
      <webElementGuid>a14eeb0a-71df-4dd5-b1db-96e412947454</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Tulis Pertanyaan']/parent::*</value>
      <webElementGuid>9c8fd24b-ba78-4cdd-823e-4438e59a25b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>4df8d662-e51d-4476-afce-45989b35eac0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Tulis Pertanyaan' or . = 'Tulis Pertanyaan')]</value>
      <webElementGuid>6653d564-ac7c-42d3-b6d0-4f7afaa9b813</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
