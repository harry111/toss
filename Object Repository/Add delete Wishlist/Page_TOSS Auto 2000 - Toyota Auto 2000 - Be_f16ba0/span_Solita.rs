<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Solita</name>
   <tag></tag>
   <elementGuidId>c88d77dd-b8a5-4715-ae6c-1c3f5bd2f568</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#dropdownMenuLinkProfile > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='dropdownMenuLinkProfile']/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>157a9e76-6e71-46ce-9417-953bf316cff5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Solita</value>
      <webElementGuid>48fa65f3-bb07-4473-bcbb-97d8c946a535</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;dropdownMenuLinkProfile&quot;)/span[1]</value>
      <webElementGuid>365dcdea-6ec2-4d1f-8401-3394dd92da98</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='dropdownMenuLinkProfile']/span</value>
      <webElementGuid>7a87d30d-e963-494d-91d0-d5c0cfba9a6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tampilkan Semua'])[1]/following::span[2]</value>
      <webElementGuid>6d6c732d-4389-48b3-822d-04c3966ecece</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notifikasi'])[1]/following::span[2]</value>
      <webElementGuid>4b394d4a-0eb7-49a5-bd53-341e8c4a86d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Akun Saya'])[1]/preceding::span[1]</value>
      <webElementGuid>86874d40-dce4-431f-af9f-3472b98ecd49</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pesanan Saya'])[1]/preceding::span[2]</value>
      <webElementGuid>8c01fc98-74f5-40d7-aad0-379cb5e2e1c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Solita']/parent::*</value>
      <webElementGuid>6d354afb-7bd8-4266-ad0c-13ffba0c1f53</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[5]/a/span</value>
      <webElementGuid>ba3069ed-29dc-4395-a285-d1077c3ef561</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Solita' or . = 'Solita')]</value>
      <webElementGuid>750fc385-b658-4414-965e-0b9777a81605</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
