<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>strong_Add to Wishlist</name>
   <tag></tag>
   <elementGuidId>ca4376be-ad82-473c-b81f-a05658ef6150</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.d-inline.v-middle.text-wishlist > strong</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/div[5]/div[5]/div/div/div[4]/a/p/strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>739b508b-8d30-4f54-a9a3-3c289ebf7069</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add to Wishlist</value>
      <webElementGuid>105f6cd8-d3e7-452c-a711-26f9d2884eff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;ps-page&quot;]/div[@class=&quot;sticky-product-price&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-6 col-lg-4 col-12 flex-middle sticky-button-wrapper flex-v-align-center&quot;]/a[@class=&quot;mr-4 c-pointer&quot;]/p[@class=&quot;d-inline v-middle text-wishlist&quot;]/strong[1]</value>
      <webElementGuid>cfcf962e-04d1-4249-8d2e-4b1266f9a0c9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div[5]/div[5]/div/div/div[4]/a/p/strong</value>
      <webElementGuid>d571f356-7c2e-4a86-b657-0a7e0ad826c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+ Tambah Add On'])[1]/following::strong[1]</value>
      <webElementGuid>c01ab9a9-cb7a-4955-9884-35a5349c1805</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 2.500.000'])[2]/following::strong[1]</value>
      <webElementGuid>10f3f186-dd3b-408c-82b1-f687f036e953</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to cart'])[1]/preceding::strong[1]</value>
      <webElementGuid>c848981f-cb32-47c7-ba5a-977ccff1be50</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checkout'])[1]/preceding::strong[1]</value>
      <webElementGuid>b036012a-f67e-4807-a39a-4e5a40aef3b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add to Wishlist']/parent::*</value>
      <webElementGuid>5afbb616-946f-41f4-8002-37b58b9c4876</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p/strong</value>
      <webElementGuid>c1db7dfd-92a6-4887-a34f-2cedaca6c2bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'Add to Wishlist' or . = 'Add to Wishlist')]</value>
      <webElementGuid>ca1641ac-740b-4ff4-bbf7-7c5a4df74bb7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
