<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>strong_Remove Wishlist</name>
   <tag></tag>
   <elementGuidId>f8231b43-7ea9-4509-b50e-2665ffd53607</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>p.d-inline.v-middle.text-wishlist > strong</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/div[9]/div/div/div[4]/a/p/strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>7878a064-4f13-41b9-a692-2e3083634b69</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Remove Wishlist</value>
      <webElementGuid>9d341e88-0359-4181-8fad-2e1cca23cf95</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;sticky-product-price&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-6 col-lg-4 col-12 flex-middle sticky-button-wrapper flex-v-align-center&quot;]/a[@class=&quot;mr-4 c-pointer&quot;]/p[@class=&quot;d-inline v-middle text-wishlist&quot;]/strong[1]</value>
      <webElementGuid>fbb8f0b8-6787-4afc-bc55-e7cdbd258816</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div[9]/div/div/div[4]/a/p/strong</value>
      <webElementGuid>6bc845f9-4829-435c-8a15-8941b4035d8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+ Tambah Add On'])[1]/following::strong[1]</value>
      <webElementGuid>a3922764-3e61-4e8b-b022-80eb77463ec3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 350.000.000'])[2]/following::strong[1]</value>
      <webElementGuid>4c06d2f5-1906-4930-908e-53b901f018e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to cart'])[1]/preceding::strong[1]</value>
      <webElementGuid>2fd266b2-e0f3-4081-a1fc-fc9b8bc1b630</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checkout'])[1]/preceding::strong[1]</value>
      <webElementGuid>96d177e1-b64f-47d0-a4d8-1ad7ba6d0da6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Remove Wishlist']/parent::*</value>
      <webElementGuid>01cb886e-a7ec-4ad9-b625-1763128761af</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p/strong</value>
      <webElementGuid>d7258738-f418-4d4f-819b-0f5745e823ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'Remove Wishlist' or . = 'Remove Wishlist')]</value>
      <webElementGuid>c5f84e9d-d940-4635-96ae-b5266d7a4416</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
