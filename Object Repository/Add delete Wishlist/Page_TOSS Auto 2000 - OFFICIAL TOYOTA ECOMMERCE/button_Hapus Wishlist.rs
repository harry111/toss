<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Hapus Wishlist</name>
   <tag></tag>
   <elementGuidId>dba8826f-18f1-4f4f-9bb5-3b60c1c041bf</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary.button-blue.mr-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='v-pills-profile']/div/div/div/div/div/div[2]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5a021ca9-380e-4dd2-bc59-88663fc13f70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary button-blue mr-4</value>
      <webElementGuid>5afe7fd3-fba9-41d5-bacc-8b70e6b2de50</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Hapus Wishlist</value>
      <webElementGuid>43f6adf4-5658-4b12-abc2-555e408c764d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;v-pills-profile&quot;)/div[1]/div[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12 col-xl-8 col-lg-8 col-md-8&quot;]/div[@class=&quot;d-flex wishlist-tools-wrapper&quot;]/div[@class=&quot;change-wishlist&quot;]/button[@class=&quot;btn btn-primary button-blue mr-4&quot;]</value>
      <webElementGuid>94fd0a8e-b916-46eb-a4fe-3e304e9ecd01</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='v-pills-profile']/div/div/div/div/div/div[2]/button</value>
      <webElementGuid>7582b5c6-7cb0-4c3f-a2c4-6bb6bab6133b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-'])[2]/following::button[1]</value>
      <webElementGuid>a2c7d474-a70a-4b60-9369-afc33d66d16c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Harga'])[1]/following::button[1]</value>
      <webElementGuid>cc1785ee-eb84-4c44-979c-e4180bda2d1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Batal'])[1]/preceding::button[1]</value>
      <webElementGuid>6503bc4b-cdea-4eda-af66-5fe2d8433fd1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Semua'])[1]/preceding::button[2]</value>
      <webElementGuid>2c5358a0-e865-488c-afb5-0bb44cfbf6db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Hapus Wishlist']/parent::*</value>
      <webElementGuid>53ee7acc-1cff-4f99-808a-48ed5460c206</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>14591f0e-07c1-4ccc-b195-92c296d63d7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Hapus Wishlist' or . = 'Hapus Wishlist')]</value>
      <webElementGuid>ff3a2793-94d6-4d62-a33d-677a2089ad73</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
