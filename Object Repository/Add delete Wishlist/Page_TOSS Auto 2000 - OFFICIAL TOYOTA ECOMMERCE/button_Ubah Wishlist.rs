<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Ubah Wishlist</name>
   <tag></tag>
   <elementGuidId>f84955c2-a2fa-46f8-9e7e-727cc58bb18e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-default</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='v-pills-profile']/div/div/div/div/div/div[2]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>835c14ac-001e-4702-a736-3a675013ea58</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default</value>
      <webElementGuid>73ead1cd-0a1f-444d-9629-1450bcba73b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ubah Wishlist</value>
      <webElementGuid>9796afc0-6ecb-4e4e-9bc2-b3e0b0423c4e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;v-pills-profile&quot;)/div[1]/div[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12 col-xl-8 col-lg-8 col-md-8&quot;]/div[@class=&quot;d-flex wishlist-tools-wrapper&quot;]/div[@class=&quot;change-wishlist&quot;]/button[@class=&quot;btn btn-default&quot;]</value>
      <webElementGuid>7462575d-697a-421d-8dd5-10e4ec1fdbe0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='v-pills-profile']/div/div/div/div/div/div[2]/button</value>
      <webElementGuid>f6891807-208b-4326-b013-89c7804970ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-'])[2]/following::button[1]</value>
      <webElementGuid>ca46d987-d478-4cd9-b755-d74e76f61b18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Harga'])[1]/following::button[1]</value>
      <webElementGuid>f073af30-6267-4539-9ff8-42a6a25095aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Urut Berdasarkan'])[1]/preceding::button[1]</value>
      <webElementGuid>75ade991-4420-4fdb-8d5c-c4f79299119a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Coiloverd'])[1]/preceding::button[1]</value>
      <webElementGuid>3d88d41b-9343-4aa0-b18a-f707c6bac617</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Ubah Wishlist']/parent::*</value>
      <webElementGuid>bdab76cc-b7fb-4804-b391-2b7217536ff4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>630c9ca1-22bf-468e-ba43-37b0ee63d738</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Ubah Wishlist' or . = 'Ubah Wishlist')]</value>
      <webElementGuid>b1d22aa0-a7a8-460f-a617-869465090969</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
