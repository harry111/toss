<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_KeranjangKeranjang Anda kosongNotifikas_3bc93a</name>
   <tag></tag>
   <elementGuidId>dc3a0ab1-2040-4837-b108-793ccc4cd146</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ps-header__right</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/header/div[2]/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>cdb87547-b698-496a-b6bf-99f1f2be29be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ps-header__right</value>
      <webElementGuid>079f07fc-1ed3-4f7b-a559-cb813c28a832</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>KeranjangKeranjang Anda kosongNotifikasiTidak ada notifikasi Masuk DaftarNo results</value>
      <webElementGuid>39f1d5fd-575f-4f2b-b371-b704fbf54373</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/header[@class=&quot;ps-header ps-header--2 ps-header--8 sticky&quot;]/div[@class=&quot;ps-header__middle&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;ps-header__right&quot;]</value>
      <webElementGuid>79e809f0-c5b7-4b14-bb71-1c053ac78e35</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/header/div[2]/div/div[2]</value>
      <webElementGuid>b21738bc-cd21-45e2-8618-a9918f8c92bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About'])[1]/following::div[4]</value>
      <webElementGuid>31ab6191-bf1d-496b-8995-8fb8b5bd7633</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Simpan Lokasi'])[1]/following::div[8]</value>
      <webElementGuid>c466dd85-bf7c-4bbc-bacf-b80b84275dd6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]</value>
      <webElementGuid>e3215738-ed51-4293-8905-586bac06f866</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'KeranjangKeranjang Anda kosongNotifikasiTidak ada notifikasi Masuk DaftarNo results' or . = 'KeranjangKeranjang Anda kosongNotifikasiTidak ada notifikasi Masuk DaftarNo results')]</value>
      <webElementGuid>8e4ed7fb-1c47-41a1-bca4-02c4aa90cc12</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
