<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img</name>
   <tag></tag>
   <elementGuidId>b92e8ca7-a5cd-4400-8901-0c4d89b087ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>img[alt=&quot;te-image_hupymrb.jpg&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/div[3]/div/div/div/div/div[2]/div[2]/div/div/div/a/div/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>51c9d13e-e968-40d7-bfc2-5acf62b5eaaf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>te-image_hupymrb.jpg</value>
      <webElementGuid>613134cd-c71f-4129-8983-880c95a3bc70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://d8xf6tpln2i96.cloudfront.net/te-image_hupymrb.jpg</value>
      <webElementGuid>b8db76b8-6dc5-44b3-9782-bcec92fbd294</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;ps-page&quot;]/div[@class=&quot;ps-categogy&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;ps-categogy__content&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12 col-md-9&quot;]/div[@class=&quot;ps-categogy--grid ps-categogy--detail mt-0&quot;]/div[@class=&quot;row m-0 mb-5&quot;]/div[@class=&quot;col-6 col-lg-3 col-sm-6 col-md-6 p-0&quot;]/div[@class=&quot;ps-product ps-product--standard br-0 ps-product-list--standard&quot;]/a[@class=&quot;ps-product__image&quot;]/div[@class=&quot;ps-product__thumbnail thumbnail-product-list&quot;]/img[1]</value>
      <webElementGuid>c5f50f63-93f8-4f4d-a0f4-448b34f19fa5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div[3]/div/div/div/div/div[2]/div[2]/div/div/div/a/div/img</value>
      <webElementGuid>01b2a3df-0023-43e2-99c9-0e9ef37a4555</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[@alt='te-image_hupymrb.jpg']</value>
      <webElementGuid>e74e81ab-2b9b-4ba6-8c3c-f829dcaf5dbf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a/div/img</value>
      <webElementGuid>6ce05a93-3215-465d-83b1-3ccfa42ff34b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@alt = 'te-image_hupymrb.jpg' and @src = 'https://d8xf6tpln2i96.cloudfront.net/te-image_hupymrb.jpg']</value>
      <webElementGuid>1794ee92-d1b3-4724-a99f-7c2aa7ca39ad</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
