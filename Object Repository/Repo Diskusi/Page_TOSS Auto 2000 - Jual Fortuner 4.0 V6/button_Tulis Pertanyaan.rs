<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Tulis Pertanyaan</name>
   <tag></tag>
   <elementGuidId>b58087cd-755f-4597-b58d-651c35d4c8a4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.radius-50.ps-btn--primary.px-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='discussion-content']/div/section[2]/div[2]/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>38e0d8fe-342f-46d5-8a76-0b3a8703b967</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn radius-50 ps-btn--primary px-4</value>
      <webElementGuid>0b98876c-71ae-4d3d-809a-3b3d6ab8b6c4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Tulis Pertanyaan</value>
      <webElementGuid>b8960ee5-2ede-4840-81ab-bd56dd13ce06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;discussion-content&quot;)/div[@class=&quot;ps-desc&quot;]/section[@class=&quot;discussion-well-segment&quot;]/div[@class=&quot;w-100 flex-between v-center m-flex-column&quot;]/button[@class=&quot;btn radius-50 ps-btn--primary px-4&quot;]</value>
      <webElementGuid>38892419-3034-4cb4-baa6-2faf6d7cca82</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='discussion-content']/div/section[2]/div[2]/button</value>
      <webElementGuid>78cfd054-0780-4e3f-afe5-1a6afc1f7f00</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskusikan dengan penjual atau pengguna lain'])[1]/following::button[1]</value>
      <webElementGuid>1603b29f-ab94-497d-878e-926ce0e4986d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ada pertanyaan?'])[1]/following::button[1]</value>
      <webElementGuid>46a42276-6a55-4f30-b258-cdf29d302d67</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Solita Saragih'])[1]/preceding::button[1]</value>
      <webElementGuid>df79b1b0-407a-4509-9a45-1eb00d7d616d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apr 27, 2023'])[1]/preceding::button[1]</value>
      <webElementGuid>3a882549-894e-49d3-b5fb-f1da80a4dcdc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Tulis Pertanyaan']/parent::*</value>
      <webElementGuid>e4053ab4-6a40-4435-ac84-db972c7bbd2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/button</value>
      <webElementGuid>1bc4e54d-f359-4a88-9680-9d615d9c339f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Tulis Pertanyaan' or . = 'Tulis Pertanyaan')]</value>
      <webElementGuid>1015652b-5c57-4e5b-b824-696b3308421b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
