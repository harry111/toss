<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Kendaraan</name>
   <tag></tag>
   <elementGuidId>210a2d2d-0dbd-4818-a25c-b4a411f8f602</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/header/div[3]/div/div/nav/ul/li/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.fa.fa-bars.has-mega-menu > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>cce950f8-6d42-41f8-84a4-0d5f6d2a9f22</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/kendaraan</value>
      <webElementGuid>0d0cf374-9866-4458-afb1-186a9f216a56</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Kendaraan</value>
      <webElementGuid>52723221-a1ee-40ac-a040-42c0f551e1c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/header[@class=&quot;ps-header ps-header--2 ps-header--8 sticky&quot;]/div[@class=&quot;ps-navigation&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;ps-navigation__left&quot;]/nav[@class=&quot;ps-main-menu&quot;]/ul[@class=&quot;menu&quot;]/li[@class=&quot;fa fa-bars has-mega-menu&quot;]/a[1]</value>
      <webElementGuid>bb267700-c8f6-41b1-b06e-23b4c823c997</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/header/div[3]/div/div/nav/ul/li/a</value>
      <webElementGuid>96a4f951-721a-44ad-bfab-3f976feb9528</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Kendaraan')]</value>
      <webElementGuid>69b3d870-c120-4f55-a9f0-cfe75be4648b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No results'])[1]/following::a[1]</value>
      <webElementGuid>38af43ff-1af6-4ef0-9336-24013cd8d633</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Keluar'])[1]/following::a[3]</value>
      <webElementGuid>2abc3775-4a8e-4d70-be32-5b95452a36e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MPV'])[1]/preceding::a[1]</value>
      <webElementGuid>a953399d-934a-4120-aa8c-9bf162fdd9b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Avanza'])[1]/preceding::a[1]</value>
      <webElementGuid>3bfc7d63-132a-4686-a45f-79bc3bd7747e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kendaraan']/parent::*</value>
      <webElementGuid>4b337356-878c-428a-bd6a-80de071c239b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/kendaraan')]</value>
      <webElementGuid>3e1f48b2-e1ff-4ae6-b746-6bd5d03128ea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//nav/ul/li/a</value>
      <webElementGuid>f8f090f6-6a7a-4457-8448-972b84af9681</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/kendaraan' and (text() = 'Kendaraan' or . = 'Kendaraan')]</value>
      <webElementGuid>040335cf-a98d-410b-87e3-144eb56146cc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
