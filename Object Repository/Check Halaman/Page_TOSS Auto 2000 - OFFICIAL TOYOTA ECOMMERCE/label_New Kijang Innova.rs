<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_New Kijang Innova</name>
   <tag></tag>
   <elementGuidId>976b5b00-accf-4b33-9a55-4939d0203e25</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/div[3]/div/div/div/div/div/div/div[3]/div/div[3]/div/label</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>5a54d3a7-9523-454d-8f99-afbb0b274104</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>custom-control-label</value>
      <webElementGuid>5a0d640d-c262-448b-a4cf-5a648e052938</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>brandTypeNewKijangInnova</value>
      <webElementGuid>d3bdfff1-2aad-41c4-acf3-0114cd37457c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>New Kijang Innova</value>
      <webElementGuid>db8f2b68-07a7-4351-a8fc-7e4890ceafbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;ps-page&quot;]/div[@class=&quot;ps-categogy&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;ps-categogy__content&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12 col-md-3&quot;]/div[@class=&quot;ps-widget ps-widget--product&quot;]/div[@class=&quot;ps-widget__block&quot;]/div[@class=&quot;ps-widget__content&quot;]/div[@class=&quot;ps-widget__item&quot;]/div[@class=&quot;custom-control custom-checkbox&quot;]/label[@class=&quot;custom-control-label&quot;]</value>
      <webElementGuid>ca9384cd-935c-4a87-8405-bd9f3d94f2b9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div[3]/div/div/div/div/div/div/div[3]/div/div[3]/div/label</value>
      <webElementGuid>0f8a5e43-65e5-489a-a13e-ff3e53b6ef38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Venturer'])[3]/following::label[1]</value>
      <webElementGuid>d925a2d0-f967-4aea-961a-ead90789d797</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Avanza'])[3]/following::label[2]</value>
      <webElementGuid>e1f723f3-40bd-4e66-9637-8af4eaac914e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Agya'])[3]/preceding::label[1]</value>
      <webElementGuid>6e0d4163-7749-4d3c-81cb-c97a6dd34338</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ALL NEW RAIZE'])[3]/preceding::label[2]</value>
      <webElementGuid>ef136374-b7a7-4f10-8a6f-0ee1897e7779</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div[3]/div/label</value>
      <webElementGuid>5bc0cb24-d5b2-4118-b89e-d073d8f75ffc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'New Kijang Innova' or . = 'New Kijang Innova')]</value>
      <webElementGuid>c2a177a1-d0da-42c4-8161-5914155821c3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
