<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Add to cart</name>
   <tag></tag>
   <elementGuidId>2431324d-18c9-4dc1-90e3-a43ff6a544ce</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='__next']/div[9]/div/div/div[4]/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.ps-btn.ps-btn--primary.btn-primary-custom-mobile</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>8a93f226-9092-4a61-b4f0-073fb427deaa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ps-btn ps-btn--primary btn-primary-custom-mobile</value>
      <webElementGuid>ab621efa-3c4c-4818-8f8a-7aa3f61d9472</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Add to cart</value>
      <webElementGuid>a0e1aa66-ef9c-423d-93c9-5d7bfeafd557</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;__next&quot;)/div[@class=&quot;sticky-product-price&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-6 col-lg-4 col-12 flex-middle sticky-button-wrapper flex-v-align-center&quot;]/button[@class=&quot;ps-btn ps-btn--primary btn-primary-custom-mobile&quot;]</value>
      <webElementGuid>55c58f2a-85fc-4887-b7b1-589fd3604504</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='__next']/div[9]/div/div/div[4]/button</value>
      <webElementGuid>05f53439-a800-4867-9e9f-c847ac3ff2ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add to Wishlist'])[1]/following::button[1]</value>
      <webElementGuid>937c7a82-310c-435f-bd76-a1fb9640bf89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+ Tambah Add On'])[1]/following::button[1]</value>
      <webElementGuid>90177d98-6180-4399-a081-14d29dcf50b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checkout'])[1]/preceding::button[1]</value>
      <webElementGuid>ab75ad11-7386-4018-b2d6-9c96f137ba24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Deskripsi'])[3]/preceding::button[2]</value>
      <webElementGuid>685ad341-dc6c-421e-9924-2aeafa52e1c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Add to cart']/parent::*</value>
      <webElementGuid>724ea860-3183-4e57-b09b-942424955984</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/button</value>
      <webElementGuid>b7b7e014-2d27-49f2-9077-09c0c443291a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Add to cart' or . = 'Add to cart')]</value>
      <webElementGuid>d3f8d096-65b7-44b4-8fd1-6f9bef9cc4ca</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
